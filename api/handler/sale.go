package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/market2075343/api_gateway/config"
	"gitlab.com/market2075343/api_gateway/genproto/branch_service"
	"gitlab.com/market2075343/api_gateway/genproto/sale_service"
	"gitlab.com/market2075343/api_gateway/pkg/helper"
)

// CreateSale godoc
// @Security ApiKeyAuth
// @Router       /v1/sales [post]
// @Summary      Create a new sale
// @Description  Create a new sale with the provided details
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        sale     body  sale_service.SaleCreateReq  true  "data of the sale"
// @Success      201  {object}  sale_service.SaleCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateSale(ctx *gin.Context) {
	var sale = sale_service.Sale{}

	err := ctx.ShouldBindJSON(&sale)
	if err != nil {
		h.handlerResponse(ctx, "CreateSale", http.StatusBadRequest, err.Error())
		return
	}

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// 3. Sale create qilishda opened statusda smena bo'ishi kerak, yo'q bo'lsa error qaytishi kerak

	respCheck, err := h.services.SmenaService().CheckSmena(ctx.Request.Context(), &branch_service.SmenaCheckReq{
		StaffId:  claims.StaffId,
		BranchId: claims.BranchId,
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().CheckSmena", http.StatusBadRequest, err.Error())
		return
	}

	if respCheck.Msg == "no" {
		h.handlerResponse(ctx, "no opened smena", http.StatusBadRequest, "no opened smena")
		return
	}

	resp, err := h.services.SaleService().Create(ctx, &sale_service.SaleCreateReq{
		BranchId:  claims.BranchId,
		CashierId: claims.StaffId,
		Price:     0,
	})

	if err != nil {
		h.handlerResponse(ctx, "SaleService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create sale response", http.StatusOK, resp)
}

// ListSales godoc
// @Router       /v1/sales [get]
// @Summary      List sales
// @Description  get sales
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Param        branch_id     query     string false "search by branch_id"
// @Param        client_name     query     string false "search by client_name"
// @Param        payment_type     query     string false "search by payment_type"
// @Param        cashier_id     query     string false "search by cashier_id"
// @Param        status     query     string false "search by status"
// @Param        created_at_from     query     string false "search by created_at_from"
// @Param        created_at_to     query     string false "search by created_at_to"
// @Param        price_from     query     string false "search by price_from"
// @Param        price_to     query     string false "search by price_to"
// @Success      200  {array}   sale_service.Sale
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListSale(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	priceFrom, err := strconv.ParseFloat(ctx.DefaultQuery("price_from", "0"), 64)
	if err != nil {
		h.handlerResponse(ctx, "error get price from", http.StatusBadRequest, err.Error())
		return
	}

	priceTo, err := strconv.ParseFloat(ctx.DefaultQuery("price_to", "999999999"), 64)
	if err != nil {
		h.handlerResponse(ctx, "error get price to", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().GetList(ctx.Request.Context(), &sale_service.SaleGetListReq{
		Page:          int64(page),
		Limit:         int64(limit),
		BranchId:      ctx.Query("branch_id"),
		ClientName:    ctx.Query("client_name"),
		PaymentType:   ctx.Query("payment_type"),
		CashierId:     ctx.Query("cashier_id"),
		Status:        ctx.Query("status"),
		CreatedAtFrom: ctx.DefaultQuery("created_at_from", "2000-01-01"),
		CreatedAtTo:   ctx.DefaultQuery("created_at_to", "2095-12-12"),
		PriceFrom:     float32(priceFrom),
		PriceTo:       float32(priceTo),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListSale", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list sale response", http.StatusOK, resp)
}

// GetSale godoc
// @Router       /v1/sales/{id} [get]
// @Summary      Get a sale by ID
// @Description  Retrieve a sale by its unique identifier
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Sale ID to retrieve"
// @Success      200  {object}  sale_service.Sale
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetSale(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.SaleService().GetById(ctx.Request.Context(), &sale_service.SaleIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error sale GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get sale response", http.StatusOK, resp)
}

// UpdateSale godoc
// @Router       /v1/sales/{id} [put]
// @Summary      Update an existing sale
// @Description  Update an existing sale with the provided details
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Sale ID to update"
// @Param        sale   body    sale_service.SaleUpdateReq  true    "Updated data for the sale"
// @Success      200  {object}  sale_service.SaleUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateSale(ctx *gin.Context) {
	var sale = sale_service.Sale{}
	sale.Id = ctx.Param("id")
	err := ctx.ShouldBindJSON(&sale)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Update(ctx.Request.Context(), &sale_service.SaleUpdateReq{
		Id:          sale.Id,
		BranchId:    sale.BranchId,
		CashierId:   sale.CashierId,
		Price:       sale.Price,
		PaymentType: sale.PaymentType,
		Status:      sale.Status,
		ClientName:  sale.ClientName,
	})

	if err != nil {
		h.handlerResponse(ctx, "error sale Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update sale response", http.StatusOK, resp.Msg)
}

// DeleteSale godoc
// @Router       /v1/sales/{id} [delete]
// @Summary      Delete a sale
// @Description  delete a sale by its unique identifier
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Sale ID to retrieve"
// @Success      200  {object}  sale_service.SaleDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteSale(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.SaleService().Delete(ctx.Request.Context(), &sale_service.SaleIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error sale Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete sale response", http.StatusOK, resp.Msg)
}

type FinishSaleReq struct {
	PaymentType string `json:"payment_type"`
	ClientName  string `json:"client_name"`
}

// SaleFinish godoc
// @Router       /v1/sales/finish/{id} [put]
// @Summary      Finish an existing sale
// @Description  Finish an existing sale with the provided details
// @Tags         sales
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Sale ID to finish"
// @Param        sale   body    FinishSaleReq  true    "Updated data for the sale"
// @Success      200  {object}  sale_service.SaleUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SaleFinish(ctx *gin.Context) {
	id := ctx.Param("id")
	var req = FinishSaleReq{}
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	sale, err := h.services.SaleService().GetById(ctx.Request.Context(), &sale_service.SaleIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetById", http.StatusNotFound, err.Error())
		return
	}

	saleProducts, err := h.services.SaleProductService().GetList(ctx, &sale_service.SaleProductGetListReq{
		Page:   1,
		Limit:  1000,
		SaleId: id,
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	var price float32 = 0.0
	for _, v := range saleProducts.SaleProducts {
		price += v.Price
	}

	resp, err := h.services.SaleService().Update(ctx.Request.Context(), &sale_service.SaleUpdateReq{
		Id:          id,
		BranchId:    sale.BranchId,
		CashierId:   sale.CashierId,
		Price:       price,
		PaymentType: req.PaymentType,
		ClientName:  req.ClientName,
		Status:      "success",
	})

	if err != nil {
		h.handlerResponse(ctx, "SaleService().Update", http.StatusBadRequest, err.Error())
		return
	}

	branchProducts, err := h.services.BranchProductService().GetList(ctx.Request.Context(), &branch_service.BranchProductGetListReq{
		Page:     1,
		Limit:    1000,
		BranchId: sale.BranchId,
	})
	if err != nil {
		h.handlerResponse(ctx, "BranchProductService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	brProducts := make(map[string]int, branchProducts.Count)
	for _, v := range branchProducts.BranchProducts {
		brProducts[v.ProductId] = int(v.Count)
	}

	for _, v := range saleProducts.SaleProducts {
		_, err := h.services.BranchProductService().Update(ctx.Request.Context(), &branch_service.BranchProductUpdateReq{
			ProductId: v.ProductId,
			BranchId:  sale.BranchId,
			Count:     int64(brProducts[v.ProductId]) - v.Quantity,
		})
		if err != nil {
			h.handlerResponse(ctx, "BranchProductService().Update", http.StatusInternalServerError, err.Error())
			return
		}
	}

	// _, err = h.services.StaffTransactionService().Create(ctx.Request.Context(), &sale_service.StaffTrCreateReq{
	// 	SaleId:    id,
	// 	StaffId:   sale.CashierId,
	// 	Typ:       "topup",
	// 	SourceTyp: "bonus",
	// 	Amount:    price,
	// 	AboutText: "some text",
	// })

	listSmena, err := h.services.SmenaService().GetList(ctx.Request.Context(), &branch_service.SmenaGetListReq{
		StaffId: sale.CashierId,
		Status:  "opened",
	})
	fmt.Println(listSmena.Smenas[0].SaleAmount + price)
	if err != nil {
		h.handlerResponse(ctx, "SmenaService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	// 2. Sale finifshed statusga o'tishida smenadagi summani hisoblash

	_, err = h.services.SmenaService().Update(ctx.Request.Context(), &branch_service.SmenaUpdateReq{
		Id:         listSmena.Smenas[0].Id,
		StaffId:    sale.CashierId,
		BranchId:   sale.BranchId,
		SaleAmount: listSmena.Smenas[0].SaleAmount + price,
		Status:     "opened",
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	for _, v := range saleProducts.SaleProducts {
		_, err = h.services.BranchProductTransactionService().Create(ctx.Request.Context(), &sale_service.BrPrTrCreateReq{
			BranchId:  sale.BranchId,
			StaffId:   sale.CashierId,
			ProductId: v.ProductId,
			Price:     v.Price,
			Typ:       "minus",
			Quantity:  v.Quantity,
		})
		if err != nil {
			h.handlerResponse(ctx, "BranchProductTransactionService().Create", http.StatusInternalServerError, err.Error())
			return
		}
	}

	if err != nil {
		h.handlerResponse(ctx, "StaffTransactionService().Create", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "SaleFinish response", http.StatusOK, resp.Msg)
}

package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market2075343/api_gateway/config"
	"gitlab.com/market2075343/api_gateway/genproto/staff_service"
	"gitlab.com/market2075343/api_gateway/pkg/helper"
	"gitlab.com/market2075343/api_gateway/pkg/logger"
)

type SignInReq struct {
	Username string
	Password string
}

type SignInResp struct {
	Token string
}

// SignIn staff handler
// @Router       /v1/auth/sign-in [post]
// @Summary      sign in staff
// @Description  api for sign in staff
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        staff    body     SignInReq  true  "data of staff"
// @Success      200  {object}  SignInResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SignIn(ctx *gin.Context) {
	var req SignInReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	staff, err := h.services.StaffService().GetByUsername(ctx, &staff_service.GetByUsernameReq{
		Username: req.Username,
	})

	if err != nil {
		h.log.Error("error while getting staff by username:", logger.Error(err))
		h.handlerResponse(ctx, "StaffService().GetByUsername", http.StatusBadRequest, err.Error())
		return
	}

	if err = helper.ComparePasswords([]byte(staff.Password), []byte(req.Password)); err != nil {
		h.log.Error("username or password incorrect")
		h.handlerResponse(ctx, "username or password incorrect", http.StatusBadRequest, err.Error())
		return
	}

	m := make(map[string]interface{})
	m["staff_id"] = staff.Id
	m["branch_id"] = staff.BranchId

	token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
	if err != nil {
		h.log.Error("error while generate jwt token", logger.Error(err))
		h.handlerResponse(ctx, "error while generate jwt token", http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusCreated, SignInResp{Token: token})
}

package handler

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/market2075343/api_gateway/config"
	"gitlab.com/market2075343/api_gateway/genproto/branch_service"
	"gitlab.com/market2075343/api_gateway/genproto/sale_service"
	"gitlab.com/market2075343/api_gateway/genproto/staff_service"
	"gitlab.com/market2075343/api_gateway/pkg/helper"
)

// CreateSmena godoc
// @Security ApiKeyAuth
// @Router       /v1/smena [post]
// @Summary      Create a new smena
// @Description  Create a new smena with the provided details
// @Tags         smena
// @Accept       json
// @Produce      json
// @Success      201  {object}  branch_service.SmenaCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateSmena(ctx *gin.Context) {

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// 4. Smena create qilishda shu filialda opened smena borligini tekshirish,
	// agar bor bo'lsa error qaytarish, yo'q  bo'lsa create qilish kerak

	respCheck, err := h.services.SmenaService().CheckSmena(ctx.Request.Context(), &branch_service.SmenaCheckReq{
		StaffId:  claims.StaffId,
		BranchId: claims.BranchId,
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().CheckSmena", http.StatusBadRequest, err.Error())
		return
	}

	if respCheck.Msg == "yes" {
		h.handlerResponse(ctx, "have a opened smena", http.StatusBadRequest, "have a opened smena")
		return
	}

	resp, err := h.services.SmenaService().Create(ctx, &branch_service.SmenaCreateReq{
		StaffId:    claims.StaffId,
		BranchId:   claims.BranchId,
		SaleAmount: 0,
		Status:     "opened",
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create smena response", http.StatusOK, resp)
}

// ListSmena godoc
// @Router       /v1/smena [get]
// @Summary      List smena
// @Description  get smena
// @Tags         smena
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Param        staff_id     query     string false "search by staff_id"
// @Param        branch_id     query     string false "search by branch_id"
// @Param        status     query     string false "search by status"
// @Success      200  {array}   branch_service.Smena
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListSmena(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().GetList(ctx.Request.Context(), &branch_service.SmenaGetListReq{
		Page:     int64(page),
		Limit:    int64(limit),
		StaffId:  ctx.Query("staff_id"),
		BranchId: ctx.Query("branch_id"),
		Status:   ctx.Query("status"),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListSmena", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list smena response", http.StatusOK, resp)
}

// GetSmena godoc
// @Router       /v1/smena/{id} [get]
// @Summary      Get a smena by ID
// @Description  Retrieve a smena by its unique identifier
// @Tags         smena
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Smena ID to retrieve"
// @Success      200  {object}  branch_service.Smena
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetSmena(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "error get smena id", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().GetById(ctx.Request.Context(), &branch_service.SmenaIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error smena GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get smena response", http.StatusOK, resp)
}

// UpdateSmena godoc
// @Router       /v1/smena/{id} [put]
// @Summary      Update an existing smena
// @Description  Update an existing smena with the provided details
// @Tags         smena
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Smena ID to update"
// @Param        smena   body    branch_service.SmenaUpdateReq  true    "Updated data for the smena"
// @Success      200  {object}  branch_service.SmenaUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateSmena(ctx *gin.Context) {
	var smena = branch_service.Smena{}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "error while parse id", http.StatusBadRequest, err.Error())
		return
	}

	smena.Id = int64(id)

	if err = ctx.ShouldBindJSON(&smena); err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().Update(ctx.Request.Context(), &branch_service.SmenaUpdateReq{
		Id:         smena.Id,
		StaffId:    smena.StaffId,
		BranchId:   smena.BranchId,
		SaleAmount: smena.SaleAmount,
		Status:     smena.Status,
	})

	if err != nil {
		h.handlerResponse(ctx, "error smena Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update smena response", http.StatusOK, resp.Msg)
}

// DeleteSmena godoc
// @Router       /v1/smena/{id} [delete]
// @Summary      Delete a smena
// @Description  delete a smena by its unique identifier
// @Tags         smena
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Smena ID to retrieve"
// @Success      200  {object}  branch_service.SmenaDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteSmena(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "error get smena id", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().Delete(ctx.Request.Context(), &branch_service.SmenaIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error smena Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete smena response", http.StatusOK, resp.Msg)
}

// 5. Smenani yopish- closed statusga o'tadi.
// Smenaga tegishli salelarni hammasi yo finished yo canceled bo'lishi kerak

// 6. Stafflarni balancega pul hisoblab berish logikasi Smenani yopishda bo'lishi kerak.
// Smenaga tegishli finished salelar uchun hisoblash kerak

// UpdateSmena godoc
// @Router       /v1/smena/close/{id} [put]
// @Summary      Close an existing smena
// @Description  Close an existing smena with the provided details
// @Tags         smena
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Smena ID to update"
// @Success      200  {object}  branch_service.SmenaUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CloseSmena(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "error get smena id", http.StatusBadRequest, err.Error())
		return
	}

	smena, err := h.services.SmenaService().GetById(ctx.Request.Context(), &branch_service.SmenaIdReq{
		Id: int64(id),
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	sales, err := h.services.SaleService().GetList(ctx.Request.Context(), &sale_service.SaleGetListReq{
		Page:          1,
		Limit:         math.MaxInt,
		BranchId:      smena.BranchId,
		CashierId:     smena.StaffId,
		CreatedAtFrom: "2000-01-02",
		CreatedAtTo:   time.Now().Add(time.Hour * 24).Format("2006-01-02"),
		PriceFrom:     0,
		PriceTo:       math.MaxFloat32,
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	var (
		totalPrice float32
		totalCount int
	)

	for _, sale := range sales.Sales {
		fmt.Println("status: ", sale.Status)
		if sale.Status == "" {
			totalPrice = 0
			h.handlerResponse(ctx, "sales not finished", http.StatusBadRequest, "sales not finished")
			return
		}
		if sale.Status == "success" {
			totalPrice += sale.Price
			totalCount += 1
		}
	}

	staff, err := h.services.StaffService().GetById(ctx.Request.Context(), &staff_service.StaffIdReq{Id: smena.StaffId})
	if err != nil {
		h.handlerResponse(ctx, "StaffService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	staffTariff, err := h.services.StaffTariffService().GetById(ctx.Request.Context(), &staff_service.TariffIdReq{
		Id: staff.TariffId,
	})

	if err != nil {
		h.handlerResponse(ctx, "StaffTariffService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	if staffTariff.Typ == "fixed" {
		_, err := h.services.StaffService().Update(ctx.Request.Context(), &staff_service.StaffUpdateReq{
			Id:        staff.Id,
			BranchId:  staff.BranchId,
			TariffId:  staff.TariffId,
			Name:      staff.Name,
			Typ:       staff.Typ,
			Balance:   staff.Balance + float32(totalCount)*staffTariff.AmountForCard,
			BirthDate: staff.BirthDate,
		})

		if err != nil {
			h.handlerResponse(ctx, "StaffService().Update", http.StatusBadRequest, err.Error())
			return
		}
	} else if staffTariff.Typ == "percent" {
		_, err := h.services.StaffService().Update(ctx.Request.Context(), &staff_service.StaffUpdateReq{
			Id:        staff.Id,
			BranchId:  staff.BranchId,
			TariffId:  staff.TariffId,
			Name:      staff.Name,
			Typ:       staff.Typ,
			Balance:   staff.Balance + totalPrice*staffTariff.AmountForCard/100,
			BirthDate: staff.BirthDate,
		})

		if err != nil {
			h.handlerResponse(ctx, "StaffService().Update", http.StatusBadRequest, err.Error())
			return
		}
	}

	_, err = h.services.SmenaService().Update(ctx.Request.Context(), &branch_service.SmenaUpdateReq{
		Id:         int64(id),
		StaffId:    smena.StaffId,
		BranchId:   smena.BranchId,
		SaleAmount: smena.SaleAmount,
		Status:     "closed",
	})

	if err != nil {
		h.handlerResponse(ctx, "SmenaService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "close smena response", http.StatusOK, "successfully closed")
}
